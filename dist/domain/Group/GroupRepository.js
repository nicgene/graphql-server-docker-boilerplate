"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const uuid_1 = require("@dupkey/uuid");
const GroupEntity_1 = require("./GroupEntity");
class GroupRepository {
    constructor(database) {
        this.database = database;
    }
    createInstance(group) {
        return new GroupEntity_1.GroupEntity(uuid_1.default.fromBuffer(group.id), group.name, group.schema, group.visibility, group.created, group.path, group.description, (group.parentId === null) ? null : uuid_1.default.fromBuffer(group.parentId), group.updated, group.scope);
    }
    async fetchAllByUserId(userId) {
        let sql = " \
      WITH RECURSIVE `cte` (`id`, `parentId`, `name`, `path`, `description`, `schema`, `visibility`, `created`, `updated`) AS ( \
        SELECT `group`.* \
        FROM `group`, `member` \
        WHERE `member`.`userId` = :userId \
          AND `member`.`kind` = 'group' \
          AND `member`.`itemId` = `group`.`id` \
        UNION DISTINCT \
          SELECT `p`.* \
          FROM `group` AS `p` \
          INNER JOIN `cte` \
            ON `p`.`parentId` = `cte`.`id` \
      ) \
      SELECT * \
      FROM `cte` \
    ";
        let results = await this.database.query(sql, { userId: userId.getBuffer() });
        return results.map((result) => this.createInstance(result));
    }
    async fetchOneById(id) {
        let sql = " \
      SELECT * \
      FROM `group` \
      WHERE `group`.`id` = :id \
    ";
        let result = await this.database.query(sql, { id: id.getBuffer() });
        if (result.length === 1) {
            return this.createInstance(result[0]);
        }
        return null;
    }
    async fetchOneByPathParentId(path, parentId) {
        let where1 = "WHERE `group`.`path` = :path";
        if (path === null) {
            where1 = "WHERE `group`.`path` IS NULL";
        }
        let parent = null;
        let where2 = " AND `group`.`parentId` = :parentId";
        if (parentId === null) {
            where2 = " AND `group`.`parentId` IS NULL";
        }
        else {
            parent = parentId.getBuffer();
        }
        let sql = 'SELECT * FROM `group` ' + where1 + where2;
        let result = await this.database.query(sql, { parentId: parent, path });
        if (result.length === 1) {
            return this.createInstance(result[0]);
        }
        return null;
    }
    async save(group) {
        let sql = " \
      INSERT INTO `group` \
      (`id`, `parentId`, `name`, `path`, `description`, `schema`, `visibility`, `created`) VALUES \
      (:id, :parentId, :name, :path, :description, :schema, :visibility, :created) \
    ";
        let values = {
            id: group.getId().getBuffer(),
            parentId: (group.getParentId() === null) ? null : group.getParentId().getBuffer(),
            name: group.getName(),
            path: group.getPath(),
            description: group.getDescription(),
            schema: group.getSchema(),
            visibility: group.getVisibility(),
            created: group.getCreated()
        };
        return await this.database.query(sql, values);
    }
    async update(group) {
        let sql = " \
      UPDATE `group` \
      SET `group`.`name` = :name, \
        `group`.`path` = :path, \
        `group`.`description` = :description, \
        `group`.`visibility` = :visibility, \
        `group`.`updated` = :updated \
      WHERE `group`.`id` = :id \
    ";
        let values = {
            id: group.getId().getBuffer(),
            name: group.getName(),
            path: group.getPath(),
            description: group.getDescription(),
            visibility: group.getVisibility(),
            updated: group.getUpdated()
        };
        return await this.database.query(sql, values);
    }
}
exports.default = GroupRepository;
//# sourceMappingURL=GroupRepository.js.map