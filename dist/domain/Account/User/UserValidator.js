"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const validator_1 = require("@dupkey/validator");
const Joi = require("joi");
class UserValidator extends validator_1.JoiValidator {
    forCreate(name, email, password) {
        let input = {
            name,
            email,
            password
        };
        let rules = {
            name: Joi.string().min(3).max(45).required(),
            email: Joi.string().email({ minDomainAtoms: 2 }).required(),
            password: Joi.string().min(8).required(),
        };
        return this.validate(input, rules);
    }
    forFetch(id) {
        let input = { id };
        let rules = {
            id: Joi.string().guid({ version: ['uuidv4'] }).required()
        };
        return this.validate(input, rules);
    }
    forUpdateActivated(id) {
        let input = { id };
        let rules = {
            id: Joi.string().guid({ version: ['uuidv4'] }).required()
        };
        return this.validate(input, rules);
    }
}
exports.default = UserValidator;
//# sourceMappingURL=UserValidator.js.map