"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const uuid_1 = require("@dupkey/uuid");
const AuthTokenEntity_1 = require("./AuthTokenEntity");
class AuthTokenRepository {
    constructor(database) {
        this.database = database;
    }
    createInstance(authToken) {
        return new AuthTokenEntity_1.default(uuid_1.default.fromBuffer(authToken.id), uuid_1.default.fromBuffer(authToken.userId), authToken.accessToken, authToken.refreshToken, authToken.created);
    }
    async deleteOneByAccessToken(accessToken) {
        let sql = " \
      DELETE FROM `account_authToken` \
      WHERE `account_authToken`.`accessToken` = :accessToken \
    ";
        let values = { accessToken };
        return await this.database.query(sql, values);
    }
    async deleteOneByRefreshToken(refreshToken) {
        let sql = " \
      DELETE FROM `account_authToken` \
      WHERE `account_authToken`.`refreshToken` = :refreshToken \
    ";
        let values = { refreshToken };
        return await this.database.query(sql, values);
    }
    async fetchOneByAccessToken(accessToken) {
        let sql = " \
      SELECT * \
      FROM `account_authToken` \
      WHERE `account_authToken`.`accessToken` = :accessToken \
    ";
        let result = await this.database.query(sql, { accessToken });
        if (result.length === 1) {
            return this.createInstance(result[0]);
        }
        return null;
    }
    async save(authToken) {
        let sql = " \
      INSERT INTO `account_authToken` \
      (id, userId, accessToken, refreshToken, created) VALUES \
      (:id, :userId, :accessToken, :refreshToken, :created) \
    ";
        let values = {
            id: authToken.getId().getBuffer(),
            userId: authToken.getUserId().getBuffer(),
            accessToken: authToken.getAccessToken(),
            refreshToken: authToken.getRefreshToken(),
            created: authToken.getCreated()
        };
        return await this.database.query(sql, values);
    }
}
exports.default = AuthTokenRepository;
//# sourceMappingURL=AuthTokenRepository.js.map