"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt_1 = require("@dupkey/jwt");
const mail_1 = require("@dupkey/mail");
const Mailgun = __importStar(require("mailgun-js"));
const message_1 = require("./message");
const jwt = new Map([
    [
        'jsonWebToken', {
            definition: jwt_1.Jwt,
            dependencies: [
                process.env.JWT_ACCESS_SECRET,
                process.env.JWT_REFRESH_SECRET
            ]
        }
    ]
]);
const mail = new Map([
    [
        'mailgun', {
            definition: Mailgun,
            dependencies: [{
                    apiKey: process.env.MAILGUN_KEY,
                    domain: process.env.MAILGUN_DOMAIN
                }]
        }
    ], [
        'transport', {
            definition: mail_1.MailgunTransport,
            dependencies: ['mailgun']
        }
    ], [
        'mailer', {
            definition: mail_1.Mailer,
            dependencies: [
                'transport',
                new mail_1.Email('support'.concat('@', String(process.env.SENDMAIL_HOST)), process.env.CLIENT_NAME),
                { client: process.env.CLIENT_NAME, uri: process.env.CLIENT_URI },
                (process.env.SENDMAIL === 'true')
            ]
        }
    ], [
        'message', {
            definition: message_1.message
        }
    ]
]);
const dependencies = new Map([
    ...jwt,
    ...mail,
]);
exports.default = dependencies;
//# sourceMappingURL=dependencies.js.map