import { ContainerBuilderInterface } from '@dupkey/container';
import { Jwt } from '@dupkey/jwt';
import { Email, Mailer, MailgunTransport } from '@dupkey/mail';
import * as Mailgun from 'mailgun-js';
import { message } from './message';

const jwt: Map<string, ContainerBuilderInterface> = new Map([
  [
    'jsonWebToken', {
      definition: Jwt,
      dependencies: [
        process.env.JWT_ACCESS_SECRET,
        process.env.JWT_REFRESH_SECRET
      ]
    }
  ]
]);

const mail: Map<string, ContainerBuilderInterface> = new Map([
  [
    'mailgun', {
      definition: Mailgun,
      dependencies: [{
        apiKey: process.env.MAILGUN_KEY,
        domain: process.env.MAILGUN_DOMAIN
      }]
    }
  ],[
    'transport', {
      definition: MailgunTransport,
      dependencies: ['mailgun']
    }
  ],[
    'mailer', {
      definition: Mailer,
      dependencies: [
        'transport',
        new Email('support'.concat('@', String(process.env.SENDMAIL_HOST)), process.env.CLIENT_NAME),
        { client: process.env.CLIENT_NAME, uri: process.env.CLIENT_URI },
        (process.env.SENDMAIL === 'true')
      ]
    }
  ],[
    'message', {
      definition: message
    }
  ]
]);

const dependencies: Map<string, ContainerBuilderInterface> = new Map([
  ...jwt,
  ...mail,
]);

export default dependencies;