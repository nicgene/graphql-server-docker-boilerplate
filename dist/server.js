"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_1 = require("apollo-server");
const schema_1 = __importDefault(require("./schema"));
const container_1 = require("@dupkey/container");
const dependencies_1 = __importDefault(require("./dependencies"));
const server = new apollo_server_1.ApolloServer({
    schema: schema_1.default,
    context: async ({ req, connection }) => {
        if (connection) {
            connection.context.container = new container_1.Container(dependencies_1.default);
            return connection.context;
        }
        else {
            let token = req.headers.authorization || req.headers.Authorization || '';
            return { token };
        }
    },
});
server.listen().then(({ url, subscriptionsUrl }) => {
    console.log(`🚀 Server ready at ${url}`);
    console.log(`🚀 Subscriptions ready at ${subscriptionsUrl}`);
});
//# sourceMappingURL=server.js.map