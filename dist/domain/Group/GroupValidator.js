"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const validator_1 = require("@dupkey/validator");
const Joi = require("joi");
class GroupValidator extends validator_1.JoiValidator {
    forCreate(name, path, visibility, description, parentId, schema) {
        let input = {
            name,
            path,
            visibility,
            description,
            parentId,
            schema
        };
        let rules = {
            name: Joi.string().min(3).max(45).required(),
            path: Joi.string().min(3).max(55).regex(/^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/, { name: 'uri' }).allow(null),
            visibility: Joi.string().valid('public', 'private').required(),
            description: Joi.string().max(191).allow(null),
            parentId: Joi.string().guid({ version: ['uuidv4'] }).allow(null),
            schema: Joi.string().min(3).max(45).required()
        };
        return this.validate(input, rules);
    }
    forFetch(id) {
        let input = { id };
        let rules = {
            id: Joi.string().guid({ version: ['uuidv4'] }).required()
        };
        return this.validate(input, rules);
    }
    forFetchByPath(path) {
        let input = { path };
        let rules = {
            path: Joi.string().regex(/^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/, { name: 'uri' }),
        };
        return this.validate(input, rules);
    }
    forUpdate(id, name, path, visibility, description) {
        let input = {
            id,
            name,
            path,
            visibility,
            description
        };
        let rules = {
            id: Joi.string().guid({ version: ['uuidv4'] }).required(),
            name: Joi.string().min(3).max(45).required(),
            path: Joi.string().min(3).max(55).regex(/^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/, { name: 'uri' }).allow(null),
            visibility: Joi.string().valid('public', 'private').required(),
            description: Joi.string().max(191).allow(null)
        };
        return this.validate(input, rules);
    }
}
exports.default = GroupValidator;
//# sourceMappingURL=GroupValidator.js.map