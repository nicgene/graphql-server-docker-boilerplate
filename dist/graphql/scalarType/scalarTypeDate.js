"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const graphql_tools_1 = require("graphql-tools");
const language_1 = require("graphql/language");
const typeDefs = `
  scalar Date
  
  type Query {
    testDate(ISOString: String!): Date
  }
`;
const resolvers = {
    Query: {
        testDate: (_, { ISOString }) => new Date(ISOString)
    },
    Date: new graphql_1.GraphQLScalarType({
        name: 'Date',
        description: 'Date/time custom scalar type.',
        parseValue: (value) => new Date(value),
        serialize: (value) => value.toISOString(),
        parseLiteral: (ast) => {
            if (ast.kind === language_1.Kind.STRING) {
                return new Date(ast.value);
            }
            return null;
        }
    })
};
exports.default = graphql_tools_1.makeExecutableSchema({
    typeDefs,
    resolvers
});
//# sourceMappingURL=scalarTypeDate.js.map