import { GraphQLScalarType } from 'graphql';
import { makeExecutableSchema } from 'graphql-tools';
import { Kind } from 'graphql/language';

const regex = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
const uuid = (value: any) => {
  return regex.test(value) ? value : null;
}

const typeDefs = `
  scalar UUID
  
  type Query {
    testUUID(UUIDString: String!): UUID
  }
`;

const resolvers = {
  Query: {
    testUUID: (_: any, { UUIDString }: { UUIDString: string }) => UUIDString
  },
  
  UUID: new GraphQLScalarType({
    name: 'UUID',
    description: 'UUID custom scalar type. [RFC 4122](https://tools.ietf.org/html/rfc4122)`',
    parseValue: uuid, // value from the client
    serialize: uuid, // value sent to the client
    parseLiteral: (ast: any) => {
      if (ast.kind === Kind.STRING) {
        return uuid(ast.value) // ast value is always in string format
      }
      return null;
    }
  })
}

export default makeExecutableSchema({
  typeDefs,
  resolvers
});