"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class MemberEntity {
    constructor(id, userId, scope, kind, itemId, created, app = null) {
        this.id = id;
        this.userId = userId;
        this.scope = scope;
        this.kind = kind;
        this.itemId = itemId;
        this.created = created;
        this.app = app;
    }
    getId() {
        return this.id;
    }
    getUserId() {
        return this.userId;
    }
    getScope() {
        return this.scope;
    }
    setScope(scope) {
        this.scope = scope;
        return this;
    }
    getKind() {
        return this.kind;
    }
    getItemId() {
        return this.itemId;
    }
    getCreated() {
        return this.created;
    }
    getApp() {
        return this.app;
    }
}
exports.MemberEntity = MemberEntity;
//# sourceMappingURL=MemberEntity.js.map