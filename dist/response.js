"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const payload_1 = require("@dupkey/payload");
const apollo_server_core_1 = require("apollo-server-core");
exports.response = (payload) => {
    switch (payload.getStatus()) {
        case 'ACCEPTED':
            payload_1.PayloadStatus.ACCEPTED;
            return payload.getOutput();
        case 'CREATED':
            payload_1.PayloadStatus.CREATED;
            return payload.getOutput();
        case 'FOUND':
            payload_1.PayloadStatus.FOUND;
            return payload.getOutput();
        case 'NOT_AUTHENTICATED':
            payload_1.PayloadStatus.NOT_AUTHENTICATED;
            return new apollo_server_core_1.AuthenticationError('Authentication required');
        case 'NOT_AUTHORIZED':
            payload_1.PayloadStatus.NOT_AUTHORIZED;
            return new apollo_server_core_1.ApolloError('Unauthorized', '401');
        case 'NOT_CREATED':
            payload_1.PayloadStatus.NOT_CREATED;
            return null;
        case 'NOT_FOUND':
            payload_1.PayloadStatus.NOT_FOUND;
            return null;
        case 'NOT_UPDATED':
            payload_1.PayloadStatus.NOT_UPDATED;
            return null;
        case 'NOT_VALID':
            payload_1.PayloadStatus.NOT_VALID;
            return new apollo_server_core_1.UserInputError('Invalid input', {
                invalidArgs: payload.getErrors()
            });
        case 'PROCESSING':
            payload_1.PayloadStatus.PROCESSING;
            return payload.getInput();
        case 'UPDATED':
            payload_1.PayloadStatus.UPDATED;
            return payload.getOutput();
        default:
            console.error('The payload status that the domain responded with does not exist in the response. Did you forget to add it?');
            return new apollo_server_core_1.ApolloError('The payload status that the domain responded with does not exist in the response. Did you forget to add it?', '500');
    }
};
//# sourceMappingURL=response.js.map