"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mail_1 = require("@dupkey/mail");
class UserMessage {
    constructor(mailer, message) {
        this.mailer = mailer;
        this.message = message;
    }
    async forCreate(user) {
        let message = new mail_1.Message(user.getEmail(), this.message.forCreate.template, 'User Registration', { id: user.getId().toString(), email: user.getEmail().getAddress() });
        return await this.mailer.send(message);
    }
    async forExists(user) {
        let message = new mail_1.Message(user.getEmail(), this.message.forExists.template, 'Account Access');
        return await this.mailer.send(message);
    }
}
exports.default = UserMessage;
//# sourceMappingURL=UserMessage.js.map