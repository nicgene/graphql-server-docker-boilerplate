"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class GroupEntity {
    constructor(id, name, schema, visibility, created, path = null, description = null, parentId = null, updated = null, scope = null) {
        this.id = id;
        this.name = name;
        this.schema = schema;
        this.visibility = visibility;
        this.created = created;
        this.path = path;
        this.description = description;
        this.parentId = parentId;
        this.updated = updated;
        this.scope = scope;
    }
    getId() {
        return this.id;
    }
    getName() {
        return this.name;
    }
    setName(name) {
        this.name = name;
        return this;
    }
    getSchema() {
        return this.schema;
    }
    getVisibility() {
        return this.visibility;
    }
    setVisibility(visibility) {
        this.visibility = visibility;
        return this;
    }
    getCreated() {
        return this.created;
    }
    setCreated(created) {
        this.created = created;
        return this;
    }
    getPath() {
        return this.path;
    }
    setPath(path) {
        this.path = path;
        return this;
    }
    getDescription() {
        return this.description;
    }
    setDescription(description) {
        this.description = description;
        return this;
    }
    getParentId() {
        return this.parentId;
    }
    setParentId(parentId) {
        this.parentId = parentId;
        return this;
    }
    getUpdated() {
        return this.updated;
    }
    setUpdated(updated) {
        this.updated = updated;
        return this;
    }
    getScope() {
        return this.scope;
    }
}
exports.GroupEntity = GroupEntity;
//# sourceMappingURL=GroupEntity.js.map