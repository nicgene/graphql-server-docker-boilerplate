"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const uuid_1 = require("@dupkey/uuid");
const MemberEntity_1 = require("./MemberEntity");
class MemberRepository {
    constructor(database) {
        this.database = database;
    }
    createInstance(member) {
        return new MemberEntity_1.MemberEntity(uuid_1.default.fromBuffer(member.id), uuid_1.default.fromBuffer(member.userId), member.scope, member.kind, uuid_1.default.fromBuffer(member.itemId), member.created);
    }
    async fetchAllByItemId(itemId) {
        let sql = " \
      SELECT * \
      FROM `member` \
      WHERE `member`.`itemId` = :itemId \
    ";
        let results = await this.database.query(sql, { itemId: itemId.getBuffer() });
        return results.map((result) => this.createInstance(result));
    }
    async fetchAllByGroupIdUserId(groupId, userId) {
        let sql = " \
      WITH RECURSIVE `cte` AS ( \
        SELECT `group`.`parentId`, `member`.* \
        FROM `group` \
        LEFT OUTER JOIN `member` \
          ON `member`.`userId` = :userId \
            AND `member`.`kind` = 'group' \
            AND `member`.`itemId` = `group`.`id` \
        WHERE `group`.`id` = :groupId \
        UNION ALL \
          SELECT `g`.`parentId`, `m`.* \
          FROM `cte` AS `c` \
          INNER JOIN `group` AS `g` \
            ON `c`.`parentId` = `g`.`id` \
          LEFT OUTER JOIN `member` AS `m` \
            ON `c`.`parentId` = `m`.`itemId` \
              AND `m`.`userId` = :userId \
      ) \
      SELECT * \
      FROM `cte` \
    ";
        let results = await this.database.query(sql, { groupId: groupId.getBuffer(), userId: userId.getBuffer() });
        results = results.filter((result) => result.scope !== null);
        return results.map((result) => this.createInstance(result));
    }
    async save(member) {
        let sql = " \
      INSERT INTO `member` \
      (id, userId, scope, kind, itemId, created) VALUES \
      (:id, :userId, :scope, :kind, :itemId, :created) \
    ";
        let values = {
            id: member.getId().getBuffer(),
            userId: member.getUserId().getBuffer(),
            scope: member.getScope(),
            kind: member.getKind(),
            itemId: member.getItemId().getBuffer(),
            created: member.getCreated()
        };
        return await this.database.query(sql, values);
    }
}
exports.default = MemberRepository;
//# sourceMappingURL=MemberRepository.js.map