"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_tools_1 = require("graphql-tools");
const checkAuth_1 = require("../checkAuth");
const response_1 = require("../response");
const typeDefs = `
  scalar Date
  scalar UUID

  enum GroupVisibility {
    private,
    public
  }

  input CreateGroupInput {
    parentId: UUID
    name: String!
    path: String
    description: String
    visibility: GroupVisibility
  }

  input UpdateGroupInput {
    id: UUID!
    name: String!
    path: String
    description: String
    visibility: GroupVisibility!
  }

  type Group {
    id: UUID!
    parentId: UUID
    name: String!
    path: String
    description: String
    visibility: GroupVisibility!
    schema: String!
    created: Date!
    updated: Date
  }

  type Mutation {
    createGroup(input: CreateGroupInput!): Group
    updateGroup(input: UpdateGroupInput!): Group
  }

  type Query {
    groupById(id: UUID!): Group
    groupsByUser: [Group]
  }
`;
const resolvers = {
    Query: {
        groupById: async (_, { id }, { context, headers }) => {
            let user = await checkAuth_1.checkAuth(context, headers, false);
            let groupService = context.container.get('groupService');
            let payload = await groupService.fetch(user.getOutput(), id);
            return response_1.response(payload);
        },
        groupsByUser: async (_, {}, { context, headers }) => {
            let user = await checkAuth_1.checkAuth(context, headers);
            let groupService = context.container.get('groupService');
            let payload = await groupService.browse(user.getOutput());
            return response_1.response(payload);
        }
    },
    Mutation: {
        createGroup: async (_, { input }, { context, headers }) => {
            let user = await checkAuth_1.checkAuth(context, headers);
            let groupService = context.container.get('groupService');
            let payload = await groupService.create(user.getOutput(), input.name, input.path || null, input.visibility || 'public', input.description || null, input.parentId || null);
            return response_1.response(payload);
        },
        updateGroup: async (_, { input }, { context, headers }) => {
            let user = await checkAuth_1.checkAuth(context, headers);
            let groupService = context.container.get('groupService');
            let payload = await groupService.update(user.getOutput(), input.id, input.name, input.path || null, input.visibility, input.description || null);
            return response_1.response(payload);
        }
    }
};
exports.default = graphql_tools_1.makeExecutableSchema({ typeDefs, resolvers });
//# sourceMappingURL=groupSchema.js.map