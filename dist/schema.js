"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_tools_1 = require("graphql-tools");
const scalarTypeDate_1 = __importDefault(require("./graphql/scalarType/scalarTypeDate"));
const scalarTypeUUID_1 = __importDefault(require("./graphql/scalarType/scalarTypeUUID"));
const typeDefs = `
  type Query {
    "A simple type for getting started!"
    hello: String
  }
`;
exports.default = graphql_tools_1.mergeSchemas({
    schemas: [
        scalarTypeDate_1.default,
        scalarTypeUUID_1.default,
        typeDefs
    ],
    resolvers: {
        Query: {
            hello: () => 'world',
        }
    }
});
//# sourceMappingURL=schema.js.map