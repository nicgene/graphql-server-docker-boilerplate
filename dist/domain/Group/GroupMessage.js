"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mail_1 = require("@dupkey/mail");
class UserMessage {
    constructor(mailer, message) {
        this.mailer = mailer;
        this.message = message;
    }
    async forCreate(group, user) {
        let message = new mail_1.Message(user.getEmail(), this.message.forCreate.template, this.message.forCreate.subject, {
            groupId: group.getId(),
            groupName: group.getName(),
            groupSchema: group.getSchema(),
            scope: 'admin'
        });
        return await this.mailer.send(message);
    }
}
exports.default = UserMessage;
//# sourceMappingURL=GroupMessage.js.map