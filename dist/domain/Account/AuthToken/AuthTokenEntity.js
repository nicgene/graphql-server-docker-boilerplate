"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AuthTokenEntity {
    constructor(id, userId, accessToken, created, refreshToken = null) {
        this.id = id;
        this.userId = userId;
        this.accessToken = accessToken;
        this.created = created;
        this.refreshToken = refreshToken;
    }
    getId() {
        return this.id;
    }
    getUserId() {
        return this.userId;
    }
    getAccessToken() {
        return this.accessToken;
    }
    getCreated() {
        return this.created;
    }
    getRefreshToken() {
        return this.refreshToken;
    }
}
exports.default = AuthTokenEntity;
//# sourceMappingURL=AuthTokenEntity.js.map