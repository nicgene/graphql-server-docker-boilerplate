import { ApolloServer } from 'apollo-server';
import schema from './schema';
import { Container } from '@dupkey/container';
import dependencies from './dependencies';

const server = new ApolloServer({
  schema,
  context: async ({ req, connection }) => {
    if (connection) {
      connection.context.container = new Container(dependencies);
      return connection.context;
    } else {
      let token = req.headers.authorization || req.headers.Authorization || '';
      return { token };
    }
  },
});

server.listen().then(({ url, subscriptionsUrl }: { url: string, subscriptionsUrl: string }) => {
  console.log(`🚀 Server ready at ${url}`);
  console.log(`🚀 Subscriptions ready at ${subscriptionsUrl}`);
});