"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_tools_1 = require("graphql-tools");
const checkAuth_1 = require("../checkAuth");
const response_1 = require("../response");
const typeDefs = `
  scalar Date
  scalar UUID

  type ActivateUserPayload {
    id: UUID!
  }

  input RegisterUserInput {
    name: String!
    email: String!
    password: String!
  }

  type User {
    id: UUID!
    name: String!
    created: Date!
    updated: Date
  }

  type Mutation {
    activateUser(id: UUID!): ActivateUserPayload
    registerUser(input: RegisterUserInput!): User
  }

  type Query {
    userById(id: UUID!): User
    userCurrent: User
  }
`;
const resolvers = {
    Query: {
        userById: async (_, { id }, { context }) => {
            let userService = context.container.get('userService');
            let payload = await userService.fetch(id);
            return response_1.response(payload);
        },
        userCurrent: async (_, {}, { context, headers }) => {
            let userPayload = await checkAuth_1.checkAuth(context, headers);
            return response_1.response(userPayload);
        }
    },
    Mutation: {
        activateUser: async (_, { id }, { context }) => {
            let userService = context.container.get('userService');
            let payload = await userService.updateActivated(id);
            return response_1.response(payload);
        },
        registerUser: async (_, { input }, { context }) => {
            let userService = context.container.get('userService');
            let payload = await userService.create(input.name, input.email, input.password);
            return response_1.response(payload);
        }
    }
};
exports.default = graphql_tools_1.makeExecutableSchema({ typeDefs, resolvers });
//# sourceMappingURL=userSchema.js.map