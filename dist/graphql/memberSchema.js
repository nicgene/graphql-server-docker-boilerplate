"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_tools_1 = require("graphql-tools");
const response_1 = require("../response");
const typeDefs = `
  scalar Date
  scalar UUID

  type Member {
    id: UUID!
    userId: String!
    scope: String!
    kind: String!
    itemId: UUID!
    created: Date!
  }

  type Mutation {
    required: Boolean
  }

  type Query {
    membersByItemId(itemId: UUID!): [Member]
  }
`;
const resolvers = {
    Query: {
        membersByItemId: async (_, { itemId }, { context }) => {
            let memberService = context.container.get('memberService');
            let payload = await memberService.fetchAllByItemId(itemId);
            return response_1.response(payload);
        }
    },
    Mutation: {}
};
exports.default = graphql_tools_1.makeExecutableSchema({ typeDefs, resolvers });
//# sourceMappingURL=memberSchema.js.map