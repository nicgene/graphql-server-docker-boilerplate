import { mergeSchemas } from 'graphql-tools';
import scalarTypeDate from './graphql/scalarType/scalarTypeDate';
import scalarTypeUUID from './graphql/scalarType/scalarTypeUUID';

const typeDefs = `
  type Query {
    "A simple type for getting started!"
    hello: String
  }
`;

export default mergeSchemas({
  schemas: [
    scalarTypeDate,
    scalarTypeUUID,
    typeDefs
  ],
  resolvers: {
    Query: {
      hello: () => 'world',
    }
  }
});