"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const validator_1 = require("@dupkey/validator");
const Joi = require("joi");
class AuthTokenValidator extends validator_1.JoiValidator {
    forUserSignin(email, password) {
        let input = {
            email,
            password
        };
        let rules = {
            email: Joi.string().email({ minDomainAtoms: 2 }).required(),
            password: Joi.string().min(8).required()
        };
        return this.validate(input, rules);
    }
}
exports.default = AuthTokenValidator;
//# sourceMappingURL=AuthTokenValidator.js.map