"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const payload_1 = require("@dupkey/payload");
const uuid_1 = __importDefault(require("@dupkey/uuid"));
const MemberEntity_1 = require("../Member/MemberEntity");
const GroupEntity_1 = require("./GroupEntity");
class GroupService {
    constructor(memberRepository, groupMessage, groupRepository, groupValidator) {
        this.memberRepository = memberRepository;
        this.groupMessage = groupMessage;
        this.groupRepository = groupRepository;
        this.groupValidator = groupValidator;
    }
    async browse(user) {
        let payload = (new payload_1.Payload()).setInput({ user });
        let groups = await this.groupRepository.fetchAllByUserId(user.getId());
        if (groups.length === 0) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_FOUND);
        }
        return payload.setOutput(groups).setStatus(payload_1.PayloadStatus.FOUND);
    }
    async create(user, name, path = null, visibility = 'public', description = null, parentId = null, schema = 'group') {
        let payload = (new payload_1.Payload()).setInput({ user, name, path, visibility, description, parentId, schema });
        if (this.groupValidator.forCreate(name, path, visibility, description, parentId, schema) === false) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_VALID)
                .setErrors(this.groupValidator.getErrors());
        }
        let parent = null;
        if (parentId !== null) {
            parent = uuid_1.default.fromString(parentId);
            let members = await this.memberRepository.fetchAllByGroupIdUserId(parent, user.getId());
            let scope = members.some((member) => member.getScope() === 'admin');
            if (scope === false) {
                return payload.setStatus(payload_1.PayloadStatus.NOT_AUTHORIZED);
            }
        }
        let group;
        if (path !== null) {
            group = await this.groupRepository.fetchOneByPathParentId(path, parent);
            if (group !== null) {
                return payload.setStatus(payload_1.PayloadStatus.NOT_VALID)
                    .setErrors([{ title: 'Path has already been taken', source: ['Group.create.path'] }]);
            }
        }
        group = new GroupEntity_1.GroupEntity(uuid_1.default.v4(), name, schema, visibility, new Date(), path, description, parent);
        await this.groupRepository.save(group);
        if (parentId === null) {
            let member = new MemberEntity_1.MemberEntity(uuid_1.default.v4(), user.getId(), 'admin', 'group', group.getId(), new Date());
            await this.memberRepository.save(member);
        }
        await this.groupMessage.forCreate(group, user);
        return payload.setStatus(payload_1.PayloadStatus.CREATED).setOutput(group);
    }
    async fetch(user, id) {
        let payload = (new payload_1.Payload()).setInput({ user, id });
        if (this.groupValidator.forFetch(id) === false) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_VALID)
                .setErrors(this.groupValidator.getErrors());
        }
        let group = await this.groupRepository.fetchOneById(uuid_1.default.fromString(id));
        if (group === null) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_FOUND);
        }
        if (group.getVisibility() === 'public') {
            return payload.setStatus(payload_1.PayloadStatus.FOUND).setOutput(group);
        }
        if (user === null) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_AUTHENTICATED);
        }
        let members = await this.memberRepository.fetchAllByGroupIdUserId(group.getId(), user.getId());
        let scope = members.some((member) => member.getScope() === 'admin' ||
            member.getScope() === 'moderator' ||
            member.getScope() === 'member');
        if (scope === false) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_AUTHORIZED);
        }
        return payload.setStatus(payload_1.PayloadStatus.FOUND).setOutput(group);
    }
    async update(user, id, name, path, visibility, description) {
        let payload = (new payload_1.Payload()).setInput({ user, id, name, path, visibility, description });
        if (this.groupValidator.forUpdate(id, name, path, visibility, description) === false) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_VALID)
                .setErrors(this.groupValidator.getErrors());
        }
        let group = await this.groupRepository.fetchOneById(uuid_1.default.fromString(id));
        if (group === null) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_FOUND);
        }
        let members = await this.memberRepository.fetchAllByGroupIdUserId(group.getId(), user.getId());
        let scope = members.some((member) => member.getScope() === 'admin');
        if (scope === false) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_AUTHORIZED);
        }
        if (path !== null) {
            let pathCheck = await this.groupRepository.fetchOneByPathParentId(path, group.getParentId());
            if (pathCheck !== null && pathCheck.getId().toString() !== group.getId().toString()) {
                return payload.setStatus(payload_1.PayloadStatus.NOT_VALID)
                    .setErrors([{ title: 'Path has already been taken', source: ['Group.update.path'] }]);
            }
        }
        group.setName(name).setPath(path).setDescription(description).setVisibility(visibility).setUpdated(new Date());
        this.groupRepository.update(group);
        return payload.setStatus(payload_1.PayloadStatus.FOUND).setOutput(group);
    }
}
exports.default = GroupService;
//# sourceMappingURL=GroupService.js.map