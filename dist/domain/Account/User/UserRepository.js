"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mail_1 = require("@dupkey/mail");
const password_1 = require("@dupkey/password");
const uuid_1 = require("@dupkey/uuid");
const UserEntity_1 = require("./UserEntity");
class UserRepository {
    constructor(database) {
        this.database = database;
    }
    createInstance(user) {
        return new UserEntity_1.default(uuid_1.default.fromBuffer(user.id), user.name, new mail_1.Email(user.email, user.name), new password_1.default(user.password), user.created, user.updated, user.activated, user.banned);
    }
    async fetchOneByEmail(email) {
        let sql = " \
      SELECT * \
      FROM `account_user` \
      WHERE `account_user`.`email` = :email \
    ";
        let result = await this.database.query(sql, { email });
        if (result.length === 1) {
            return this.createInstance(result[0]);
        }
        return null;
    }
    async fetchOneById(id) {
        let sql = " \
      SELECT * \
      FROM `account_user` \
      WHERE `account_user`.`id` = :id \
    ";
        let result = await this.database.query(sql, { id: id.getBuffer() });
        if (result.length === 1) {
            return this.createInstance(result[0]);
        }
        return null;
    }
    async save(user) {
        let sql = " \
      INSERT INTO `account_user` \
      (id, name, email, password, created) VALUES \
      (:id, :name, :email, :password, :created) \
    ";
        let values = {
            id: user.getId().getBuffer(),
            name: user.getName(),
            email: user.getEmail().getAddress(),
            password: await user.getPassword().getHash(),
            created: user.getCreated()
        };
        return await this.database.query(sql, values);
    }
    async updateActivated(user) {
        let sql = " \
      UPDATE `account_user` \
      SET `account_user`.`activated` = :activated \
      WHERE `account_user`.`id` = :id \
    ";
        let values = {
            id: user.getId().getBuffer(),
            activated: user.getActivated()
        };
        return await this.database.query(sql, values);
    }
}
exports.default = UserRepository;
//# sourceMappingURL=UserRepository.js.map