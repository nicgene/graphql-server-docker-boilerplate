"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const graphql_tools_1 = require("graphql-tools");
const language_1 = require("graphql/language");
const regex = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
const uuid = (value) => {
    return regex.test(value) ? value : null;
};
const typeDefs = `
  scalar UUID
  
  type Query {
    testUUID(UUIDString: String!): UUID
  }
`;
const resolvers = {
    Query: {
        testUUID: (_, { UUIDString }) => UUIDString
    },
    UUID: new graphql_1.GraphQLScalarType({
        name: 'UUID',
        description: 'UUID custom scalar type. [RFC 4122](https://tools.ietf.org/html/rfc4122)`',
        parseValue: uuid,
        serialize: uuid,
        parseLiteral: (ast) => {
            if (ast.kind === language_1.Kind.STRING) {
                return uuid(ast.value);
            }
            return null;
        }
    })
};
exports.default = graphql_tools_1.makeExecutableSchema({
    typeDefs,
    resolvers
});
//# sourceMappingURL=scalarTypeUUID.js.map