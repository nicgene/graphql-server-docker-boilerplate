# graphql-server-docker-boilerplate

## Setup

## Run service offline for local development

```bash
docker-compose up
```

### Re-build the image

```bash
docker-compose up --build
```

## Run the container

```bash
docker run --name <container> -p 4000:4000 -d dupkey/graphql-server
```

## Build the image

```bash
docker build -t dupkey/graphql-server .
```

## Notes

### You can access the shells for our two images with the following (example) commands

```bash
docker exec -it <container> /bin/ash
```

```bash
docker exec -it <container> /bin/ash
```

`/bin/ash` is Ash ([Almquist Shell](https://www.in-ulm.de/~mascheck/various/ash/#busybox)) provided by BusyBox

## Todo

- Add updateParentId/path method to group
- Encrypted env variables
- Websockets
- Container need async/await
- Password buffer has been deprecated in Node 10
