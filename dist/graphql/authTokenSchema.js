"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_tools_1 = require("graphql-tools");
const response_1 = require("../response");
const typeDefs = `
  scalar Date
  scalar UUID

  type AuthToken {
    id: UUID!
    userId: UUID!
    accessToken: String!
    refreshToken: String
    created: Date
  }

  type LogoutUserPayload {
    accessToken: String!
  }

  input SigninUserInput {
    email: String!
    password: String!
  }

  type Mutation {
    logoutUser(accessToken: String!): LogoutUserPayload
    signinUser(input: SigninUserInput!): AuthToken
  }

  type Query {
    required: Boolean
  }
`;
const resolvers = {
    Mutation: {
        logoutUser: async (_, { accessToken }, { context }) => {
            let authTokenService = context.container.get('authTokenService');
            let payload = await authTokenService.userLogout(accessToken);
            return response_1.response(payload);
        },
        signinUser: async (_, { input }, { context }) => {
            let authTokenService = context.container.get('authTokenService');
            let payload = await authTokenService.userSignin(input.email, input.password);
            return response_1.response(payload);
        }
    }
};
exports.default = graphql_tools_1.makeExecutableSchema({ typeDefs, resolvers });
//# sourceMappingURL=authTokenSchema.js.map