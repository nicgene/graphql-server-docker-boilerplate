"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const payload_1 = require("@dupkey/payload");
const apollo_server_1 = require("apollo-server");
exports.checkAuth = async (context, headers, required = true) => {
    let payload = (new payload_1.Payload()).setInput({ context, required });
    let token = headers.authorization || headers.Authorization;
    if (token === undefined && required === true) {
        throw new apollo_server_1.AuthenticationError('You must supply a JWT for authentication!');
    }
    if (token === undefined && required === false) {
        return payload.setStatus(payload_1.PayloadStatus.ACCEPTED).setOutput(null);
    }
    token = token.replace('Bearer ', '');
    let jsonWebToken = context.container.get('jsonWebToken');
    let decoded = jsonWebToken.verifyAccessToken(token);
    let authTokenService = context.container.get('authTokenService');
    let authToken = await authTokenService.checkAuth(token);
    if (authToken.getStatus() === payload_1.PayloadStatus.NOT_FOUND) {
        throw new apollo_server_1.AuthenticationError('JWT was revoked!');
    }
    if (authToken.getOutput().getUserId().toString() !== decoded.id) {
        throw new apollo_server_1.AuthenticationError('User/token mismatch!');
    }
    let userService = context.container.get('userService');
    let user = await userService.fetch(decoded.id);
    if (user.getStatus() === payload_1.PayloadStatus.NOT_FOUND) {
        throw new apollo_server_1.AuthenticationError('Invalid user!');
    }
    return user;
};
//# sourceMappingURL=checkAuth.js.map