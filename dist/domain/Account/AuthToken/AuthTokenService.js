"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const password_1 = __importDefault(require("@dupkey/password"));
const payload_1 = require("@dupkey/payload");
const uuid_1 = __importDefault(require("@dupkey/uuid"));
const AuthTokenEntity_1 = __importDefault(require("./AuthTokenEntity"));
class AuthTokenService {
    constructor(authTokenRepository, authTokenValidator, jsonWebToken, userRepository) {
        this.authTokenRepository = authTokenRepository;
        this.authTokenValidator = authTokenValidator;
        this.jsonWebToken = jsonWebToken;
        this.userRepository = userRepository;
    }
    async checkAuth(accessToken) {
        let payload = (new payload_1.Payload).setInput({ accessToken });
        let token = await this.authTokenRepository.fetchOneByAccessToken(accessToken);
        if (token === null) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_FOUND);
        }
        return payload.setStatus(payload_1.PayloadStatus.FOUND).setOutput(token);
    }
    async update(refreshToken) {
        let payload = (new payload_1.Payload).setInput({ refreshToken });
        let jsonWebToken = this.jsonWebToken.verifyRefreshToken(refreshToken);
        let user = await this.userRepository.fetchOneById(uuid_1.default.fromString(jsonWebToken.id));
        if (user === null ||
            await this.authTokenRepository.deleteOneByRefreshToken(refreshToken) === null) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_FOUND);
        }
        let authToken = new AuthTokenEntity_1.default(uuid_1.default.v4(), user.getId(), this.jsonWebToken.accessToken({ id: user.getId().toString() }), new Date(), this.jsonWebToken.refreshToken({ id: user.getId().toString() }));
        await this.authTokenRepository.save(authToken);
        return payload.setStatus(payload_1.PayloadStatus.CREATED).setOutput(authToken);
    }
    async userLogout(accessToken) {
        let payload = (new payload_1.Payload).setInput({ accessToken });
        await this.authTokenRepository.deleteOneByAccessToken(accessToken);
        return payload.setStatus(payload_1.PayloadStatus.PROCESSING);
    }
    async userSignin(email, password) {
        let payload = (new payload_1.Payload).setInput({ email, password });
        if (this.authTokenValidator.forUserSignin(email, password) === false) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_VALID)
                .setErrors(this.authTokenValidator.getErrors());
        }
        let user = await this.userRepository.fetchOneByEmail(email);
        if (user === null ||
            user.getActivated() === null ||
            user.getBanned() !== null ||
            await (new password_1.default(password)).isValid(String(user.getPassword())) === false) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_FOUND);
        }
        let authToken = new AuthTokenEntity_1.default(uuid_1.default.v4(), user.getId(), this.jsonWebToken.accessToken({ id: user.getId().toString() }), new Date());
        await this.authTokenRepository.save(authToken);
        return payload.setStatus(payload_1.PayloadStatus.CREATED).setOutput(authToken);
    }
}
exports.default = AuthTokenService;
//# sourceMappingURL=AuthTokenService.js.map