"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mail_1 = require("@dupkey/mail");
const password_1 = require("@dupkey/password");
const payload_1 = require("@dupkey/payload");
const uuid_1 = require("@dupkey/uuid");
const UserEntity_1 = require("./UserEntity");
class UserService {
    constructor(userMessage, userRepository, userValidator) {
        this.userMessage = userMessage;
        this.userRepository = userRepository;
        this.userValidator = userValidator;
    }
    async create(name, email, password) {
        let payload = (new payload_1.Payload()).setInput({ name, email, password });
        if (this.userValidator.forCreate(name, email, password) === false) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_VALID)
                .setErrors(this.userValidator.getErrors());
        }
        let user = await this.userRepository.fetchOneByEmail(email);
        if (user !== null) {
            if (user.getBanned() === null) {
                await this.userMessage.forExists(user);
            }
            return payload.setStatus(payload_1.PayloadStatus.ACCEPTED)
                .setOutput(new UserEntity_1.default(uuid_1.default.v4(), name, new mail_1.Email(email, name), new password_1.default(password), new Date()));
        }
        user = new UserEntity_1.default(uuid_1.default.v4(), name, new mail_1.Email(email, name), new password_1.default(password), new Date());
        await this.userRepository.save(user);
        await this.userMessage.forCreate(user);
        return payload.setStatus(payload_1.PayloadStatus.CREATED).setOutput(user);
    }
    async fetch(id) {
        let payload = (new payload_1.Payload()).setInput({ id });
        if (this.userValidator.forFetch(id) === false) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_VALID)
                .setErrors(this.userValidator.getErrors());
        }
        let user = await this.userRepository.fetchOneById(uuid_1.default.fromString(id));
        if (user === null ||
            user.getActivated() === null ||
            user.getBanned() !== null) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_FOUND);
        }
        return payload.setStatus(payload_1.PayloadStatus.FOUND).setOutput(user);
    }
    async updateActivated(id) {
        let payload = (new payload_1.Payload()).setInput({ id });
        if (this.userValidator.forUpdateActivated(id) === false) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_VALID)
                .setErrors(this.userValidator.getErrors());
        }
        let user = await this.userRepository.fetchOneById(uuid_1.default.fromString(id));
        if (user === null ||
            user.getActivated() !== null ||
            user.getBanned() !== null) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_UPDATED);
        }
        await this.userRepository.updateActivated(user.setActivated(new Date()));
        return payload.setStatus(payload_1.PayloadStatus.UPDATED).setOutput(user);
    }
}
exports.default = UserService;
//# sourceMappingURL=UserService.js.map