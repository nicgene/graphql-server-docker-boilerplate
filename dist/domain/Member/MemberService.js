"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const payload_1 = require("@dupkey/payload");
const uuid_1 = require("@dupkey/uuid");
class MemberService {
    constructor(memberRepository) {
        this.memberRepository = memberRepository;
    }
    async fetchAllByItemId(itemId) {
        let payload = (new payload_1.Payload()).setInput({ itemId });
        let members = await this.memberRepository.fetchAllByItemId(uuid_1.default.fromString(itemId));
        if (members.length === 0) {
            return payload.setStatus(payload_1.PayloadStatus.NOT_FOUND);
        }
        return payload.setOutput(members).setStatus(payload_1.PayloadStatus.FOUND);
    }
}
exports.default = MemberService;
//# sourceMappingURL=MemberService.js.map