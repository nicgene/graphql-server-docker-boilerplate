import { MessageSettings } from '@dupkey/mail';

interface Message {
  [key: string]: MessageSettings;
}

export const message: Message = {
  // user: {
  //   forCreate: {
  //     template: 'usermessage.forcreate',
  //     subject: 'User Registration'
  //   },
  //   forExists: {
  //     template: 'usermessage.forexists',
  //     subject: 'Account Access'
  //   }
  // }
}