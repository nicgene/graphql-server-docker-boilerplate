"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class UserEntity {
    constructor(id, name, email, password, created, updated = null, activated = null, banned = null) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.created = created;
        this.updated = updated;
        this.activated = activated;
        this.banned = banned;
    }
    getId() {
        return this.id;
    }
    getName() {
        return this.name;
    }
    setName(name) {
        this.name = name;
        return this;
    }
    getEmail() {
        return this.email;
    }
    getPassword() {
        return this.password;
    }
    setPassword(password) {
        this.password = password;
        return this;
    }
    getCreated() {
        return this.created;
    }
    getUpdated() {
        return this.updated;
    }
    setUpdated(updated) {
        this.updated = updated;
        return this;
    }
    getActivated() {
        return this.activated;
    }
    setActivated(activated) {
        this.activated = activated;
        return this;
    }
    getBanned() {
        return this.banned;
    }
    setBanned(banned) {
        this.banned = banned;
        return this;
    }
}
exports.default = UserEntity;
//# sourceMappingURL=UserEntity.js.map